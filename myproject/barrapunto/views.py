from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from .models import Tabla
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from urllib import request

# Create your views here.

titulares = ""

formulario = """
<form action="" method="post">
    CMS Barrapunto <br/>
    <br />
    Name: <input name="name" type="text" value=""/><br />
    <br />
    Content: <input name="content" type="text" value=""/><br/>
    <br/>
    <button type="submit">Enviar</button><br />
    <br />
</form>
"""


def parser():

    class CounterHandler(ContentHandler):

        def __init__(self):
            self.inItem = 0
            self.inContent = 0
            self.theContent = ""
            self.theTitle = ""
            self.link = ""

        def startElement(self, name, attrs):
            if name == 'item':
                self.inItem = 1
            elif name == 'title':
                if self.inItem == 1:
                    self.inContent = 1
            elif name == 'link':
                if self.inItem == 1:
                    self.inContent = 1

        def endElement(self, name):
            global titulares
            if name == 'item':
                self.inItem = 0
            if self.inItem == 1:
                if name == 'title':
                    self.theTitle = self.theContent
                    self.inContent = 0
                    self.theContent = ""
                if name == 'link':
                    titulares += '<a href="' + self.theContent + '">' + self.theTitle + '</a><br/>'
                    self.inContent = 0
                    self.theContent = ""

        def characters(self, chars):
            if self.inContent:
                self.theContent = self.theContent + chars

    BarrapuntoParser = make_parser()
    BarrapuntoHandler = CounterHandler()
    BarrapuntoParser.setContentHandler(BarrapuntoHandler)

    xmlFile = request.urlopen("http://barrapunto.com/index.rss")
    BarrapuntoParser.parse(xmlFile)

    return titulares


@csrf_exempt
def main(request):

    if request.method == "GET":

        response = "<b1>" + formulario + "<b1/>" + parser()
        return HttpResponse(response)

    if request.method == "POST":

        nombre = request.POST['name']
        contenido = request.POST['content']
        if Tabla.objects.filter(name=nombre).exists():
            datos = Tabla.objects.get(name=nombre)
            datos.content = contenido
            datos.save()
            response = "Content has ben updated"
        else:

            datos = Tabla(name=nombre, content=contenido)
            datos.save()
            response = "Content has been added"

        return HttpResponse(response)


def addcontent(request, nombre):

    if Tabla.objects.filter(name=nombre).exists():

        corres = Tabla.objects.get(name=nombre).content
        response = "Contenido: " + str(corres) + "<br/>" + parser()
        return HttpResponse(response)

    else:
        response = "Name not found"
        return HttpResponseBadRequest(response)

